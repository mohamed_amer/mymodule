<?php

namespace Drupal\mymodule\Form;

use Drupal\node\Entity\Node;
use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class MyForm extends FormBase {
    public function getFormId()
    {
        return 'MyForm_email_form';
    }
    public function buildForm(array $form, FormStateInterface $form_state)
    {

        $form['title']=array(
            '#title'=> $this->t('Title'),
            '#type'=> 'textfield',
            '#size'=> 25,
            '#description'=> $this->t('This is a title'),
            '#required'=> TRUE,
        );
        $form['description']=array(
            '#title'=> $this->t('Description'),
            '#type'=> 'textarea',
            '#description'=> $this->t('This is a description'),
            '#required'=> TRUE,
        );
        $form['tag']=array(
            '#title'=> $this->t('Tags'),
            '#type'=> 'textfield',
            '#description'=> $this->t('This is a tag'),
        );

        // $options=array('tag1','tag2','tag3');
        //  $form['rsvplist_types'] = array(
        //      '#type' => 'select',
        //      '#title' => $this->t('The content types to enable RSVP collection for'),
        //      //'#default_value' => $config->get('allowed_types'),
        //      '#options' => $options,
        //      '#description' => $this->t('On the specified node types, an RSVP option will be available and can be enabled while tht node is being edited.'),
        //    );

        $form['submit']=array(
            '#type'=> 'submit',
            '#value'=> $this->t('Add Content'),
        );


        return $form;
    }
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $title= $form_state->getValue('title');
        $description= $form_state->getValue('description');
        $tag= $form_state->getValue('tag');
        $node= Node::create(['type'=>'article']);
        $node->set('title', $title);
        $node->set('body', $description);

        $new_term = \Drupal\taxonomy\Entity\Term::create([
            'vid' => 'field_tags',
            'name' => $tag,
         ]);

        $new_term->enforceIsNew();
        $new_term->save();
        $node->set('field_tags', $new_term->id());
        $node->save();

        \Drupal::messenger()->addMessage($this->t('Data successfully added'));
    }
}

?>